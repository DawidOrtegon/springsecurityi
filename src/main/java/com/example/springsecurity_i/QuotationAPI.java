package com.example.springsecurity_i;

import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;

@RestController
public class QuotationAPI
{
    private List<Quotation> quotations;

    public QuotationAPI() {
        this.quotations = new ArrayList<>();
        quotations.add(new Quotation("No hay que ir para atrás ni para darse impulso", "Lao Tsé"));
        quotations.add(new Quotation("Haz el amor y no la guerra", "John Lennon"));
    }

    @GetMapping("/api")
    public List<Quotation> getQuotations ()
    {
        return quotations;
    }

    @PostMapping("/api")
    public boolean addQuotations(@RequestBody Quotation quotation)
    {
        return quotations.add(quotation) ;
    }

    @DeleteMapping("/api")
    public void deleteQuotation(@RequestParam int index)
    {
        quotations.remove(index) ;
    }


}
